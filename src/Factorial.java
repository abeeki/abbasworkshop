
public class Factorial {
	public int iterativeFactorial(int n) {
		
		int factorial =1;
		int counter =1;
		while (counter<n) {
			factorial = factorial * counter;
			counter++;
		}
		
		return factorial;
	}
	public static int recursiveFactorial(int n) {
		if (n==0) {
			return 1;
		}
		
		return n*recursiveFactorial(n-1);
	}
	public static void main(String[]args) {
		System.out.println("Hello BU!");
		System.out.println(recursiveFactorial(5));
		Factorial instance = new Factorial();
		System.out.println(instance.iterativeFactorial(5));
	}
	
	
}
